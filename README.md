# 👋 Hello

Thank you for taking time out to be a part of this research activity.

To participate, you need to fork this project into your own namespace.

Below are the tasks they you are required to perform as a part of this session:

## 𝍌 Task 
In our scenario today, you are a software developer on a large team that uses a mono-repo for their projects. There is a CI/CD pipeline set up to streamline the workflow of the developers and contributors.

a. You have been assigned the task to include the changes from a feature branch `new-feature-branch` to the main branch. You have to ensure your code passes the pre-defined code quality checks included in the pipeline configuration when checked against the main branch before integration. (The feature branch already exists in the repository to help you kick-off the task).

b. In the process, observe the health of your running pipeline. If there’s any abnormality in the behaviour, figure out what is causing it.

[Link to documentation](https://docs.gitlab.com/ee/ci/quick_start/)




-------

🙏

